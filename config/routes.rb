Rails.application.routes.draw do
	scope "(:locale)", locale: /en|fa/ do
		resources :posts
		# devise_for :users
		devise_for :users, :controllers => { :registrations => 'registrations' }
		# For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

		# root to: "users#new"
		root to: "page#index"
		# get '/users/:id', to 'users'
		get 'users/:username' => 'users#show'
		post 'relationship/follow' => 'relationship#follow'
		post 'relationship/unfollow' => 'relationship#unfollow'
		# get 'friends/' => 'relationship#show_friends'

		# get 'friends/', to: 'relationship#show_friends'
		get 'friends' => 'friends#index'

	end
end
