class Post < ApplicationRecord
	validates :username, presence: true
	validates :context, presence: true, length: { maximum: 140 }

	belongs_to :user
end
