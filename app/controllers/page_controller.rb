class PageController < ApplicationController
	def index

		if current_user != nil

			@posts = []
			current_user.followings.each do |f|
				@posts = @posts + f.posts
			end
			@posts = @posts.sort_by(&:updated_at).reverse
		end

	end
end
