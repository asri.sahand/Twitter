class RelationshipController < ApplicationController

	# follow another user
	def follow
		user = User.find(params[:user_id])
		current_user.follow(user)
		redirect_to "/users/" + user.username
	end

	def unfollow
		user = User.find(params[:user_id])
		current_user.unfollow(user)
		redirect_to "/users/" + user.username
	end

end
