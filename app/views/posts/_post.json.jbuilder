json.extract! post, :id, :username, :context, :created_at, :updated_at
json.url post_url(post, format: :json)
